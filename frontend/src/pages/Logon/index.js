import React from 'react';
import { Link } from 'react-router-dom';
import { FiLogIn } from 'react-icons/fi';
import './style.scss';

import herosImage from '../../assets/images/heroes.png';
import herosLogo from '../../assets/images/logo.svg';

export default function Logon() {
	return (
		<div className="logon-container">
			<section className="form">
				<img className="logo" src={herosLogo} alt="Be The Hero" />

				<form action="/profile">
					<h1>Faça seu logon</h1>
					<input placeholder="Sua ID" />
					<button className="button" type="submit">Entrar</button>

					<Link to="/register" className="link">
						<FiLogIn size={16} color="#E02041" />
					Não tenho cadastro</Link>
				</form>

			</section>
			<img className="heroes" src={herosImage} alt="Heroes" />
		</div>
	)
}