import React from 'react';
import { Link } from 'react-router-dom';
import { FiPower, FiTrash2 } from 'react-icons/fi'
import './style.scss';

import herosLogo from '../../assets/images/logo.svg';

export default function Profile() {
	return (
		<div className="profile-container">

			<header>
				<img className="logo" src={herosLogo} alt="Be The Hero" />
				<span>Bem vindo<em>(a)</em>, APAE</span>
				<Link to="/incidents/new" className="button">Cadastrar novo caso</Link>
				<button>
					<FiPower size={16} color="#E02041" />
				</button>
			</header>

			<h1>Casos cadastrados</h1>

			<ul>
				<li>
					<strong>Caso:</strong>
					<p>Caso teste</p>

					<strong>Descrição:</strong>
					<p>Descrição Teste</p>

					<strong>Valor:</strong>
					<p>R$ 120,00</p>

					<button>
						<FiTrash2 size={16} color="#a8a8b3" />
					</button>
				</li>

				<li>
					<strong>Caso:</strong>
					<p>Caso teste</p>

					<strong>Descrição:</strong>
					<p>Descrição Teste</p>

					<strong>Valor:</strong>
					<p>R$ 120,00</p>

					<button>
						<FiTrash2 size={16} color="#a8a8b3" />
					</button>
				</li>

				<li>
					<strong>Caso:</strong>
					<p>Caso teste</p>

					<strong>Descrição:</strong>
					<p>Descrição Teste</p>

					<strong>Valor:</strong>
					<p>R$ 120,00</p>

					<button>
						<FiTrash2 size={16} color="#a8a8b3" />
					</button>
				</li>

				<li>
					<strong>Caso:</strong>
					<p>Caso teste</p>

					<strong>Descrição:</strong>
					<p>Descrição Teste</p>

					<strong>Valor:</strong>
					<p>R$ 120,00</p>

					<button>
						<FiTrash2 size={16} color="#a8a8b3" />
					</button>
				</li>
			</ul>
		</div>
	)
}