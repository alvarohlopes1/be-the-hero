import React from 'react';
import { Link } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import './style.scss';

import herosLogo from '../../assets/images/logo.svg';

export default function NewIncident() {
	return (
		<div className="new-incident-container">
			<div className="content">
				<section>
					<img className="logo" src={herosLogo} alt="Be The Hero" />
					<h1>Cadastrar novo caso</h1>
					<p>Descreva o caso detalhadamente para encontrar um herói para resolver isso.</p>

					<Link to="/" className="link">
						<FiArrowLeft size={16} color="#E02041" />voltar para a home
					</Link>
				</section>

				<form>
					<input placeholder="Titulo do caso" />
					<textarea placeholder="descrição" />
					<input placeholder="Valor em reais" />
					<div className="button-group">
						<Link to="/profile" className="button line" style={{ width: 250 }}>Cancelar</Link>
						<button className="button" type="submit">Cadastrar</button>
					</div>

				</form>

			</div>
		</div>
	)
}