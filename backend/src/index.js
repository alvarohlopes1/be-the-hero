const express = require('express');
const cors = require('cors');
const routes = require('./rotes');

const app = express();

app.use(cors(
    // { origin: 'http://meuapp.com.br' }
))
app.use(express.json());
app.use(routes);

/**
 * Anotações de aula _____________________________________________
 *
 * Tipos de Parâmetros:
 *
 * Query Params: Parâmetros nomeados enviados na rota após "?" (Usado em Filtros e paginação);
 * Route Params: Parâmetros utilizados para identificar recursos;
 * Request Body: Corpo da requisição, utilizado para criar ou alterar recursos.
 *
 * Frameworks Usados:
 * Nodemon: Node live reload
 * Knex: SQL Builder
 *
 * Creação das migrations
 * knex migrate:make nome da migrate
 *
 */

app.listen(3333);